#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


#define N 15000   // Constante N mis à la valeur 
#define M 11  // Constante M mis à la valeur 11 représentant la longueur maximum++1 d'une chaine

typedef char tab[N][M] ;    // Définition d'un tableau à double dimension[N][M]




/********************************************
 *      Génération des chaines aléatoires   * 
 ********************************************/
// Fonction permettant de générer un nombre entre 5 et 10 qui sera la longueur d'une chaine
int longueurAlea(){
    int res;
        res= rand() % 6 + 5;   //Génère un nombre entre 5 et 10
        return res;
}

// Procédure qui affiche le tableau envoyé en paramètre
void afficher(tab t){
    for (int i=0;i<N;i++){
        printf("%s ", t[i]);
    } 
}

// Fonction permettant la génération d'un nombre entre 97 et 122 permettant de créer les lettre de a à z grâce à la table ascii.
char lettreAlea(){
  return (rand() % 26 + 97 );
}

// Cette Procédure permet la création du tableau de chaine de caractères avec des longueur comprise entre 5 et 10 et des caractères allant de a à z.
void tabChaine(tab t){
  int i,j;
  int longueur;

  for (i=0;i<N;i++){
    longueur=longueurAlea();
    for (j=0;j<longueur;j++){
      t[i][j]=lettreAlea();
    }
    t[i][longueur]='\0';
  }
  
}



/********************
 *      Triage      * 
 ********************/
 // Procédure qui tri par ordre croissant en ayant en paramètre le tableau t qui sera modifier (sauf si déjà trié)
void croissant(tab t){  
    int i,j ; // Compteurs
    char temp[M]; // Varriable temporaire
    for (i=0;i<N;i++){
        j=i;
        while ((j>0) && (strcmp(t[j],t[j-1])<0)){
            strcpy(temp,t[j]);
            strcpy(t[j],t[j-1]);
            strcpy(t[j-1],temp);
            j--;
        }
    }
}

// Procédure qui tri par ordre décroissant en ayant en paramètre le tableau t qui sera modifier (sauf si déjè trié)
void decroissant(tab t){ 
    int i,j ; // Compteurs
    char temp[M]; // Variable temporaire
    for (i=0;i<N;i++){
        j=i;
        while ((j>0) && (strcmp(t[j],t[j-1])>0)){ 
            strcpy(temp,t[j]);
            strcpy(t[j],t[j-1]);
            strcpy(t[j-1],temp);
            j--;
        }
    }
}



/***************************
*    Programme principal   *
****************************/
int main(){
    tab t;
    srand(time(NULL));

    tabChaine(t);// Initialisation du tableau à double dimension avec les valeurs

    printf("Tri par insertion :\n\n");

    printf("Cas moyen :\n");

    printf("\nDebut du tri croissant :\n");
    croissant(t);
    printf("Fin du tri croissant\n");
    
    printf("\n\nCas favorable :\n");
    printf("Debut du tri croissant :\n");
    croissant(t);
    printf("Fin du tri croissant\n\n");

    printf("\nCas defavorable :\n");
    printf("Debut du tri decroissant\n");
    decroissant(t);
    printf("Fin du tri decroissant\n");
}


//strcat(str1, str2);

/* strcmp(ch1,ch2) --> 0 si ch1=ch2
                  --> >0 si ch1>ch2
                  --> <0 si ch1<ch2
*/
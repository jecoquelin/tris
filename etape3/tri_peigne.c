#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>


#define N 150000
#define J 11
typedef char tableau[N][J];

int aleatoire_longueur();
int aleatoire();
void ajouter(tableau tab);
void afficher(tableau tab);
void triPeigne(tableau tab,int val);
void triPeigneInverse(tableau tab,int val);

int main(){
  srand(time(NULL));
  tableau tab;
  //tableau tmp;
  ajouter(tab);
  

  
  printf("    TRI A PEIGNE    \n");
  printf("\ntableau avant le tri : \n");
  afficher(tab);
  int i=N;
  triPeigne(tab,i);
  printf("\ntableau après le tri : \n");
  afficher(tab);
  triPeigneInverse(tab,i);
  printf("\ntableau après le tri inverse : \n");
  afficher(tab);
}

int aleatoire(){
  int res;
  res=rand()% 26+97;
  return res ;
}

int aleatoire_longueur(){
  int res;
  res=rand()% 6+5;
  return res ;
}

void ajouter(tableau tab){
    int val;
    int longueur;
    int i,j;
    for (i=0;i<N;i++){
      longueur=aleatoire_longueur();
      for (j=0 ; j<longueur ; j++){
          val=aleatoire();
          tab[i][j]=val;
      }
      tab[i][longueur]='\0';
  }
}

void afficher(tableau tab){
  printf("\n");
  for (int i=0;i<N;i++){
      printf("%s ", tab[i]);
  }
  printf("\n");
}



/**
 * \brief procédure permettant de trier en mode croissant un tableau de la façon tri peigne 
 * 
 * \param tab : représente un tableau de valeurs
 * \param val : représente l'intervalle de la taille du tableau 
 */
void triPeigne(tableau tab,int val){
    int j;
    char temp[J];
    val=val/1.3; // Permet de diviser l'intervalle de la taille du tableau par 1.3 (c'est une division entière)
    while (val!=0){ // pour le dernier tour , val sera égal a 0 et donc c'est pour sa qu'ont l'enlève
        for (j=val;j<N;j++){
            
            if (strcmp(tab[j-val],tab[j])>0){ // regarde si le premier nombre a l'intervalle j-val est plus grand que l'intervalle j 
              strcpy(temp,tab[j-val]);
              strcpy(tab[j-val],tab[j]);  // permut les valeurs 
              strcpy(tab[j],temp); 
            }
            
        }
    val=val/1.3; // nous divisons avant de relancer le while sinon la while fait une boucle infinie
    }
}

/**
 * @brief procédure permettant de trier en mode décroissant un tableau de la façon tri peigne 
 * 
 * @param tab : représente un tableau de valeurs
 * @param val : représente l'intervalle de la taille du tableau 
 */
void triPeigneInverse(tableau tab,int val){
    int j;
    char temp[J];
    val=val/1.3; // Permet de diviser l'intervalle de la taille du tableau par 1.3 (c'est une division entière)
    while (val!=0){ // pour le dernier tour , val sera égal a 0 et donc c'est pour sa qu'ont l'enlève
        for (j=val;j<N;j++){
            
            if (strcmp(tab[j-val],tab[j])<0){  // regarde si le premier nombre a l'intervalle j-val est plus petit que l'intervalle j 
                
              strcpy(temp,tab[j-val]);
              strcpy(tab[j-val],tab[j]);  // permut les valeurs 
              strcpy(tab[j],temp);
                
            }
            
        }
    val=val/1.3; // nous divisons avant de relancer le while sinon la while fait une boucle infinie
    }
}

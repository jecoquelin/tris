#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>


#define N 150000
#define J 11
typedef char tableau[N][J];
int aleatoire_longueur();
int aleatoire();
void ajouter(tableau tab);
void afficher(tableau tab);
void triFusion(int i , int j , tableau tab, tableau tmp);
void triFusionInverse(int i , int j , tableau tab, tableau tmp);

int main(){
  srand(time(NULL));
  tableau tab;
  tableau tmp;
  
  int i=0;
  int j=N-1;
  printf("    TRI FUSION    \n");
  printf("\ntableau avant le tri : \n");
  ajouter(tab);
  afficher(tab);
  printf("\ntableau apres le tri : \n");
  triFusion(i,j,tab,tmp);
  afficher(tab);
  
  printf("\ntableau apres le tri 2 : \n");
  triFusionInverse(i,j,tab,tmp);
  afficher(tab);
}

int aleatoire(){
  int res;
  res=rand()% 26+97;
  return res ;
}

int aleatoire_longueur(){
  int res;
  res=rand()% 6+5;
  return res ;
}

void ajouter(tableau tab){
    int val;
    int longueur;
    int i,j;
    for (i=0;i<N;i++){
      longueur=aleatoire_longueur();
      for (j=0 ; j<longueur ; j++){
          val=aleatoire();
          tab[i][j]=val;
      }
      tab[i][longueur]='\0';

    
  }
}




void afficher(tableau tab){
  printf("\n");
  for (int i=0;i<N;i++){
      printf("%s ", tab[i]);
  }
  printf("\n");
}


void triFusion(int i, int j ,tableau tab, tableau tmp ){
    if (j<=i){
        return;
    }

    int m = (i+j)/2;

    triFusion(i,m,tab,tmp);     //trier la moitié gauche récursivement
    triFusion(m+1,j,tab,tmp);   //trier la moitié droite récursivement
    int pg = i;     //pg pointe au début du sous-tableau de gauche
    int pd = m+1;   //pd pointe au début du sous-tableau de droite
    int cmp;        //compteur
// on boucle de i à j pour remplir chaque élément du tableau final fusionné
    for (cmp=i;cmp<= j;cmp++){
        if (pg == m+1){ //le pointeur du sous-tableau de gauche a atteint la limite
            strcpy(tmp[cmp],tab[pd]);
            //tmp[cmp] = tab[pd];    pg == m+1
            pd++;
        }
        else if (pd == j+1){    //le pointeur du sous-tableau de droite a atteint la limite
            strcpy(tmp[cmp], tab[pg]);
            //tmp[cmp] = tab[pg];
            pg++;
        }
        else if (strcmp(tab[pg],tab[pd])<0){ //le pointeur du sous-tableau de gauche pointe vers un élément plus petit
            strcpy(tmp[cmp],tab[pg]);
            //tmp[cmp] = tab[pg];     tab[pg]< tab[pd]
            pg++;
        }
        else {  //le pointeur du sous-tableau de droite pointe vers un élément plus petit
            strcpy(tmp[cmp],tab[pd]);
            //tmp[cmp] = tab[pd];
            pd++;
        }
    }
    
    
    for(cmp = i; cmp <= j; cmp++){ //copier les éléments de tmp[] à tab[]
        strcpy(tab[cmp],tmp[cmp]);
        //tab[cmp] = tmp[cmp];
    }
}

void triFusionInverse(int i, int j ,tableau tab, tableau tmp ){
    if (j<=i){
        return;
    }

    int m = (i+j)/2;
    triFusionInverse(i,m,tab,tmp);     //trier la moitié gauche récursivement
    triFusionInverse(m+1,j,tab,tmp);   //trier la moitié droite récursivement
    int pg = i;     //pg pointe au début du sous-tableau de gauche
    int pd = m+1;   //pd pointe au début du sous-tableau de droite
    int cmp;        //compteur
// on boucle de i à j pour remplir chaque élément du tableau final fusionné
    for (cmp=i;cmp<= j;cmp++){
        if (pg == m+1){ //le pointeur du sous-tableau de gauche a atteint la limite
            strcpy(tmp[cmp],tab[pd]);
            pd++;
        }
        else if (pd == j+1){    //le pointeur du sous-tableau de droite a atteint la limite
            strcpy(tmp[cmp], tab[pg]);
            pg++;
        }
        else if (strcmp(tab[pg],tab[pd])>0){ //le pointeur du sous-tableau de gauche pointe vers un élément plus grand
            strcpy(tmp[cmp],tab[pg]);
            pg++;
        }
        else {  //le pointeur du sous-tableau de droite pointe vers un élément plus petit
            strcpy(tmp[cmp],tab[pd]);
            pd++;
        }
    }
    for(cmp = i; cmp <= j; cmp++){ //copier les éléments de tmp[] à tab[]
        strcpy(tab[cmp],tmp[cmp]);
    }
}

#include <stdlib.h>
#include <stdio.h>
#include <time.h>


#define N 2000000
typedef int tableau[N];


void afficherTableau(tableau t);
void remplirTableau(tableau t);
void triParTas(tableau t);
void tamiser(tableau t,int noeud , int n);

int nbcomp=0;
int nbpermut=0;

int main(){
    time_t debuttout = time(NULL);
    clock_t beginall = clock();
    srand(time(NULL));
    tableau t;
    remplirTableau(t);
    afficherTableau(t);

    clock_t begin = clock();
    time_t debut = time(NULL);
    triParTas(t);
    time_t fin = time(NULL);
    clock_t end = clock();
    double  tmpsCPUtri = (end - begin)*1.0 / CLOCKS_PER_SEC;
    printf("\n\n");
    afficherTableau(t);
    time_t fintout = time(NULL);
    clock_t endall = clock();
    double  tmpsCPU = (endall - beginall)*1.0 / CLOCKS_PER_SEC;
    printf("\n\nLe nombre de comparaison est %d",nbcomp);
    printf("\nLe nombre de permutation est %d\n\n",nbpermut);
    printf("Duree du programme = %.3f secondes\n", difftime(fintout, debuttout) );
    printf("Duree du tri = %.3f secondes\n", difftime(fin, debut) );
    printf("Le temps CPU du programme = %.3f secondes\n",tmpsCPU);
    printf("Le temps CPU du tri = %.3f secondes\n",tmpsCPUtri);
    return 0;
}

void afficherTableau(tableau t){
    for (int i=0;i<N;i++){
        printf("%d ",t[i]);
    }
}

void remplirTableau(tableau t){
    for(int i=0;i>N;i++){
        t[i]=rand() % RAND_MAX;
    }
}

void tamiser(tableau t, int noeud , int n){
    int temp;
    int fils=0;
    fils = 2*noeud+1;
    nbcomp++;
    if ( (fils < n) && (t[fils+1] > t[fils]) ){
        fils = fils + 1;
        
    }
    nbcomp++;
    if ( (fils <= n) && (t[noeud] < t[fils]) ){
        
        /*Permutation*/
        temp = t[fils];
        t[fils]=t[noeud];
        t[noeud]=temp;
        nbpermut++;
        tamiser(t,fils,n);
    }
}

void triParTas(tableau t){
    int temp;
    int i;
    for(i=N/2-1;i>=0;i--){
        tamiser(t,i,N-1);
    }
    for(i=N-1;i>=0;i--){
        nbpermut++;
        temp = t[0];
        t[0] = t[i];
        t[i] = temp;
        tamiser(t,0,i-1);
    }
}


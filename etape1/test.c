#include <stdlib.h>
#include <stdio.h>

#define MAX 10
typedef int tableau[MAX];

void afficher(tableau tab);

void triFusion(int i , int j , tableau tab, tableau tmp);


int main(){
    tableau tab={99,47,54,12,25,39,18,26,81,39};
    tableau tmp;
    int i=0;
    int j=MAX-1;
    printf("    TRI FUSION    \n");
    printf("\ntableau avant le tri : \n");
    afficher(tab);
    printf("\ntableau apres le tri : \n");
    triFusion(i,j,tab,tmp);
    afficher(tab);
    return EXIT_SUCCESS;
}

void afficher(tableau tab){
    for (int i=0;i< MAX; i++){
        printf("%d",tab[i]);
        printf(" ");
    }
}



void triFusion(int i, int j ,tableau tab, tableau tmp ){
    if (j<=i){
        return;
    }

    int m = (i+j)/2;

    triFusion(i,m,tab,tmp);     //trier la moitié gauche récursivement
    triFusion(m+1,j,tab,tmp);   //trier la moitié droite récursivement
    int pg = i;     //pg pointe au début du sous-tableau de gauche
    int pd = m+1;   //pd pointe au début du sous-tableau de droite
    int cmp;        //compteur
// on boucle de i à j pour remplir chaque élément du tableau final fusionné
    for (cmp=i;cmp<= j;cmp++){
        if (pg == m+1){ //le pointeur du sous-tableau de gauche a atteint la limite
            tmp[cmp] = tab[pd];
            pd++;
        }
        else if (pd == j+1){    //le pointeur du sous-tableau de droite a atteint la limite
            tmp[cmp] = tab[pg];
            pg++;
        }
        else if (tab[pg]> tab[pd]){ //le pointeur du sous-tableau de gauche pointe vers un élément plus petit
            tmp[cmp] = tab[pg];
            pg++;
        }
        else {  //le pointeur du sous-tableau de droite pointe vers un élément plus petit
            tmp[cmp] = tab[pd];
            pd++;
        }
    }
    for(cmp = i; cmp <= j; cmp++){ //copier les éléments de tmp[] à tab[]
        tab[cmp] = tmp[cmp];
    }
}
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


#define N 10    // Constante N mis à la valeur 10
typedef int tab[N] ;    // Définition d'un tableau de N entier

void croissant(tab t){  // Procédure qui tri par ordre croissant en ayant en paramètre le tableau t qui sera modifier (sauf si déjà trié)
    int i,j ; // Compteurs
    int temp; // Varriable temporaire
    for (i=0;i<N;i++){
        j=i;
        while ((j>0) && (t[j]<t[j-1])){
            temp=t[j];
            t[j]=t[j-1];
            t[j-1]=temp;
            j--;
        }
    }
}

void decroissant(tab t){ // Procédure qui tri par ordre décroissant en ayant en paramètre le tableau t qui sera modifier (sauf si déjè trié)
    int i,j ; // Compteurs
    int temp; // Variable temporaire
    for (i=0;i<N;i++){
        j=i;
        while ((j>0) && (t[j]>t[j-1])){
            temp=t[j];
            t[j]=t[j-1];
            t[j-1]=temp;
            j--;
        }
    }
}



int main(){ // Programme principal
    time_t debuttout = time(NULL);
    clock_t beginall = clock();

    int i; // compteur
    

    printf("Tri par insertion :\n\ntableau avant le tri :\n");
    tab t = {99,47,54,12,25,39,18,26,81,39} ;   //Inititialisation du tableau avec les N valeaurs

    // Affiche le tableau avant tri
    for (i=0;i<N;i++){
        printf("%d ", t[i]);
    }
    

    
    clock_t begin = clock();
    time_t debut = time(NULL);

    // Fait appel à la procédure croissant(avec le tableau t en paramètre) 
    croissant(t);
    time_t fin = time(NULL);
    clock_t end = clock();
    double  tmpsCPUtri = (end - begin)*1.0 / CLOCKS_PER_SEC;

    // Affiche le tableau dans l'ordre croissant
    printf("\nPar ordre croissant\n");
    for (i=0;i<N;i++){
        printf("%d ", t[i]);
    }
    clock_t beginde = clock();
    time_t debutde = time(NULL);

    // Fait appel à la procédure decroissant(avec le tableau t en paramètre)
    decroissant(t);
    //comptage de secondes

    time_t finde = time(NULL);
    clock_t endde = clock();
    double  tmpsCPUtride = (endde - beginde)*1.0 / CLOCKS_PER_SEC;
    // affiche le tableau dans l'ordre décroissant

    printf("\nPar ordre décroissant :\n");
    for (i=0;i<N;i++){
        printf("%d ", t[i]);
    }
    printf("\n");
    time_t fintout = time(NULL);
    clock_t endall = clock();
    double  tmpsCPU = (endall - beginall)*1.0 / CLOCKS_PER_SEC;
    printf("Duree du tri croissant = %.3f secondes\n", difftime(fin, debut) );
    printf("Duree du tri decroissant = %.3f secondes\n", difftime(finde, debutde) );
    printf("Le temps CPU du tri croissant = %.3f secondes\n",tmpsCPUtri);
    printf("Le temps CPU du tri décroissant = %.3f secondes\n",tmpsCPUtride);
    printf("Duree du programme = %.3f secondes\n", difftime(fintout, debuttout) );
    printf("Le temps CPU du programme = %.3f secondes\n",tmpsCPU);
}
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#define MAX 10
typedef int tableau[MAX];


void afficher(tableau tab);
void triPeigne(tableau tab,int val);
void triPeigneInverse(tableau tab,int val);

int main(){
    //tableau tab={99,47,54,12,25,39,18,26,81,39};
    //tableau tab1={12,18,25,26,39,39,47,54,81,99};
    time_t debuttout = time(NULL);
    clock_t beginall = clock();
    tableau tab2={99,81,54,47,39,39,26,25,18,12};
    printf("    TRI A PEIGNE    \n");
    printf("\ntableau avant le tri : \n");
    afficher(tab2);
    int i=MAX;

    clock_t begin = clock();
    time_t debut = time(NULL);

    triPeigne(tab2,i);

    time_t fin = time(NULL);
    clock_t end = clock();
    double  tmpsCPUtri = (end - begin)*1.0 / CLOCKS_PER_SEC;

    printf("\ntableau après le tri : \n");
    afficher(tab2);

    clock_t beginde = clock();
    time_t debutde = time(NULL);

    triPeigneInverse(tab2,i);

    time_t finde = time(NULL);
    clock_t endde = clock();
    double  tmpsCPUtride = (endde - beginde)*1.0 / CLOCKS_PER_SEC;

    printf("\ntableau après le tri inverse : \n");
    afficher(tab2);
    time_t fintout = time(NULL);
    clock_t endall = clock();
    double  tmpsCPU = (endall - beginall)*1.0 / CLOCKS_PER_SEC;

    printf("Duree du tri croissant = %.3f secondes\n", difftime(fin, debut) );
    printf("Duree du tri decroissant = %.3f secondes\n", difftime(finde, debutde) );
    printf("Le temps CPU du tri croissant = %.3f secondes\n",tmpsCPUtri);
    printf("Le temps CPU du tri décroissant = %.3f secondes\n",tmpsCPUtride);
    printf("Duree du programme = %.3f secondes\n", difftime(fintout, debuttout) );
    printf("Le temps CPU du programme = %.3f secondes\n",tmpsCPU);
}


void afficher(tableau tab){
    for (int i=0;i< MAX; i++){
        printf("%d",tab[i]);
        printf(" ");
    }
}

/**
 * \brief procédure permettant de trier en mode croissant un tableau de la façon tri peigne 
 * 
 * \param tab : représente un tableau de valeurs
 * \param val : représente l'intervalle de la taille du tableau 
 */
void triPeigne(tableau tab,int val){
    int j;
    int temp;
    val=val/1.3; // Permet de diviser l'intervalle de la taille du tableau par 1.3 (c'est une division entière)
    while (val!=0){ // pour le dernier tour , val sera égal a 0 et donc c'est pour sa qu'ont l'enlève
        for (j=val;j<MAX;j++){
            
            if (tab[j-val]>tab[j]){ // regarde si le premier nombre a l'intervalle j-val est plus grand que l'intervalle j 
                
                temp = tab[j-val];  // permut les valeurs 
                tab[j-val] = tab[j];
                tab[j]= temp;
                
            }
            
        }
    val=val/1.3; // nous divisons avant de relancer le while sinon la while fait une boucle infinie
    }
}

/**
 * @brief procédure permettant de trier en mode décroissant un tableau de la façon tri peigne 
 * 
 * @param tab : représente un tableau de valeurs
 * @param val : représente l'intervalle de la taille du tableau 
 */
void triPeigneInverse(tableau tab,int val){
    int j;
    int temp;
    val=val/1.3; // Permet de diviser l'intervalle de la taille du tableau par 1.3 (c'est une division entière)
    while (val!=0){ // pour le dernier tour , val sera égal a 0 et donc c'est pour sa qu'ont l'enlève
        for (j=val;j<MAX;j++){
            
            if (tab[j-val]<tab[j]){  // regarde si le premier nombre a l'intervalle j-val est plus petit que l'intervalle j 
                
                temp = tab[j-val];  // permut les valeurs 
                tab[j-val] = tab[j];
                tab[j]= temp;
                
            }
            
        }
    val=val/1.3; // nous divisons avant de relancer le while sinon la while fait une boucle infinie
    }
}

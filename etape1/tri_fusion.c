#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define MAX 10
typedef int tableau[MAX];

void afficher(tableau tab);

void triFusion(int i , int j , tableau tab, tableau tmp);
void triFusionInverse(int i , int j , tableau tab, tableau tmp);

int main(){
    //tableau tab={99,47,54,12,25,39,18,26,81,39};
    //tableau tab1={12,18,25,26,39,39,47,54,81,99};
    time_t debuttout = time(NULL);
    clock_t beginall = clock();
    tableau tab2={99,81,54,47,39,39,26,25,18,12};
    tableau tmp;
    int i=0;
    int j=MAX-1;
    printf("    TRI FUSION    \n");
    printf("\ntableau avant le tri : \n");
    afficher(tab2);
    printf("\ntableau apres le tri : \n");

    clock_t begin = clock();
    time_t debut = time(NULL);

    triFusion(i,j,tab2,tmp);

    time_t fin = time(NULL);
    clock_t end = clock();
    double  tmpsCPUtri = (end - begin)*1.0 / CLOCKS_PER_SEC;

    afficher(tab2);
    printf("\ntableau apres le tri 2 : \n");

    clock_t beginde = clock();
    time_t debutde = time(NULL);

    triFusionInverse(i,j,tab2,tmp);

    time_t finde = time(NULL);
    clock_t endde = clock();
    double  tmpsCPUtride = (endde - beginde)*1.0 / CLOCKS_PER_SEC;

    afficher(tab2);
    time_t fintout = time(NULL);
    clock_t endall = clock();
    double  tmpsCPU = (endall - beginall)*1.0 / CLOCKS_PER_SEC;
    printf("Duree du tri croissant = %.3f secondes\n", difftime(fin, debut) );
    printf("Duree du tri decroissant = %.3f secondes\n", difftime(finde, debutde) );
    printf("Le temps CPU du tri croissant = %.3f secondes\n",tmpsCPUtri);
    printf("Le temps CPU du tri décroissant = %.3f secondes\n",tmpsCPUtride);
    printf("Duree du programme = %.3f secondes\n", difftime(fintout, debuttout) );
    printf("Le temps CPU du programme = %.3f secondes\n",tmpsCPU);
    return EXIT_SUCCESS;
}

void afficher(tableau tab){
    for (int i=0;i< MAX; i++){
        printf("%d",tab[i]);
        printf(" ");
    }
}


void triFusion(int i, int j ,tableau tab, tableau tmp ){
    if (j<=i){
        return;
    }

    int m = (i+j)/2;

    triFusion(i,m,tab,tmp);     //trier la moitié gauche récursivement
    triFusion(m+1,j,tab,tmp);   //trier la moitié droite récursivement
    int pg = i;     //pg pointe au début du sous-tableau de gauche
    int pd = m+1;   //pd pointe au début du sous-tableau de droite
    int cmp;        //compteur
// on boucle de i à j pour remplir chaque élément du tableau final fusionné
    for (cmp=i;cmp<= j;cmp++){
        if (pg == m+1){ //le pointeur du sous-tableau de gauche a atteint la limite
            tmp[cmp] = tab[pd];
            pd++;
        }
        else if (pd == j+1){    //le pointeur du sous-tableau de droite a atteint la limite
            tmp[cmp] = tab[pg];
            pg++;
        }
        else if (tab[pg]< tab[pd]){ //le pointeur du sous-tableau de gauche pointe vers un élément plus petit
            tmp[cmp] = tab[pg];
            pg++;
        }
        else {  //le pointeur du sous-tableau de droite pointe vers un élément plus petit
            tmp[cmp] = tab[pd];
            pd++;
        }
    }
    for(cmp = i; cmp <= j; cmp++){ //copier les éléments de tmp[] à tab[]
        tab[cmp] = tmp[cmp];
    }
}

void triFusionInverse(int i, int j ,tableau tab, tableau tmp ){
    if (j<=i){
        return;
    }

    int m = (i+j)/2;
    triFusionInverse(i,m,tab,tmp);     //trier la moitié gauche récursivement
    triFusionInverse(m+1,j,tab,tmp);   //trier la moitié droite récursivement
    int pg = i;     //pg pointe au début du sous-tableau de gauche
    int pd = m+1;   //pd pointe au début du sous-tableau de droite
    int cmp;        //compteur
// on boucle de i à j pour remplir chaque élément du tableau final fusionné
    for (cmp=i;cmp<= j;cmp++){
        if (pg == m+1){ //le pointeur du sous-tableau de gauche a atteint la limite
            tmp[cmp] = tab[pd];
            pd++;
        }
        else if (pd == j+1){    //le pointeur du sous-tableau de droite a atteint la limite
            tmp[cmp] = tab[pg];
            pg++;
        }
        else if (tab[pg]> tab[pd]){ //le pointeur du sous-tableau de gauche pointe vers un élément plus grand
            tmp[cmp] = tab[pg];
            pg++;
        }
        else {  //le pointeur du sous-tableau de droite pointe vers un élément plus petit
            tmp[cmp] = tab[pd];
            pd++;
        }
    }
    for(cmp = i; cmp <= j; cmp++){ //copier les éléments de tmp[] à tab[]
        tab[cmp] = tmp[cmp];
    }
}




# Tris

Ce dépôt contient différents tris avec différentes donnée d'entrée.

## Contexte
Lors du BUT informatique, nous avons eu des projets à réaliser. Celui-ci est l'un deux.

## Description
Cette SAE a eu pour but de nous apprendre les différents tris et de voir lesquels était plus performant que ça soit sur des nombres ou encore des chaînes de caractère. Ces tris ont été réalisés en langage C.

La SAE 1.02 des tris s'est faite en 6 étapes :

1. Tri de 10 entiers
2. Tri de 500 000 entiers
3. Tri de chaînes de caractères
4. Comparaison des performances (temps CPU, nombre d'échange, taille du programme)
5. Données réelles
6. Évaluations individuelles (tp final)

Cette SAE m'a ainsi permis de comprendre des algorithmes plus ou moins optimisé pour trier que ça soit des nombres ou des chaîne de caractères.

## Apprentissage critique

AC 12.01 Analyser un problème avec méthode (découpage en éléments algorithmiques simples, structure de données...)<br>
AC 12.02 Comparer des algorithmes pour des problèmes classiques (tris simples, recherche...)

## Langage et Framework 
  * C

## Prérequis 
Afin de pouvoir exécuter l'application sur votre poste, vous devrez avoir :
  * Gcc

## Exécution
Pour lancer les différents tris, il vous suffit de compiler le code source et de le lancer à l'aide de cette commande `gcc -W -Wall -o tris.exe tris.c`.


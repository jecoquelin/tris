#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>

#define MAX 17776

typedef char chaine[10];

typedef struct 
{
    int dep;        //Numéro du département (22,29,35 ou 56)
    chaine date;    //date sous la forme aaaa-mm-jj
                    //exemple : 2020-05-23
    int pos;        //nombre de cas enregistrés ce jour-là
                    //dans cette classe d'âge et dans ce département
    int classe;     // tranche d’âge. La valeur 9 représente la
                    // tranche d’âge [0 - 9 ans], la valeur 19
                    // représente la tranche d’âge [10 – 19 ans],
                    // etc. jusqu’à la valeur 90 qui représente les
                    // 90 ans ou plus.
                    // Attention, la tranche d’âge 0 est un
                    // récapitulatif qui
    int pop;        // nombre d’habitants de cette tranche d’âge
                    // dans ce département
}covid;

typedef covid tab[MAX];
typedef int tableau[11];

void afficher(tab t);
void lireFichier(tab t,int *nbEnregistrement);
void triDepartement(tab t);
int population22(tab t);
void triDate(tab t,int val);
void DebutAnnee35(tab t);

int main(){
    int nbEnregistrement=0;
    tab t;
    tab tmp;
    int val=MAX; 
    lireFichier(t,&nbEnregistrement);
    
    for (int i=0;i<MAX;i++){
        tmp[i]=t[i];
    } 

    clock_t beginQ3se = clock();
    int res1 =population22(t);
    clock_t endQ3se = clock();
    double  tmpsCPUseq = (endQ3se - beginQ3se)*1.0 / CLOCKS_PER_SEC; 
    
    clock_t begin_Q3_trie = clock();
    triDepartement(tmp);
    clock_t begin_Q3_tri = clock();
    int res2 = population22(tmp);
    clock_t end_Q3_tri = clock();
    double tmpsCPUsanstri = (end_Q3_tri - begin_Q3_tri)*1.0 / CLOCKS_PER_SEC;
    clock_t end_Q3_trie = clock();
    double tmpsCPUtri = (end_Q3_trie - begin_Q3_trie)*1.0 / CLOCKS_PER_SEC;
    printf("%d\n",res1);
    printf("%d\n",res2);
    
    
    printf("%d\n",population22(t));
    triDate(t,val);
    DebutAnnee35(t);

    printf("Sequentielle %.3f\n",tmpsCPUseq);
    printf("Sans tri %.3f\n",tmpsCPUsanstri);
    printf("Avec tri %.3f\n",tmpsCPUtri);

    return 0;

}

void lireFichier(tab t,int *nbEnregistrement){
    int i=0;
    
    FILE * fic ;
    covid temp;
    fic = fopen("DonneesCovid.data.data","rb");
    fread(&temp,sizeof(covid),1,fic);

    while(!feof(fic)){
        t[i]=temp;
        /*
        printf("%d",t[i].dep);
        printf(" ");
        printf("%s",t[i].date);
        printf(" ");
        printf("%d",t[i].pos);
        printf(" ");
        printf("%d",t[i].classe);
        printf(" ");
        printf("%d",t[i].pop);
        printf("\n");*/
        
        i++;
        fread(&temp,sizeof(covid),1,fic);
    }
    *nbEnregistrement = i;
    fclose(fic);
}
void triDepartement(tab t){

    int i,j ; // Compteurs
    covid temp; // Varriable temporaire
    for (i=0;i<MAX;i++){
        j=i;
        while ((j>0) && (t[j].dep<t[j-1].dep)){
            temp=t[j];
            t[j]=t[j-1];
            t[j-1]=temp;
            j--;
        }
    }
}
void afficher(tab t){
    for (int i=0;i< MAX; i++){
        
        printf("%d",t[i].dep);
        printf(" ");
        printf("%s",t[i].date);
        printf(" ");
        printf("%d",t[i].pos);
        printf(" ");
        printf("%d",t[i].classe);
        printf(" ");
        printf("%d",t[i].pop);
        printf("\n");
        }
       
}


int population22(tab t){
    int i=0;
    int departement=22;
    int pop=0;
    bool trouve=false;

    while (!trouve && i<MAX)
    {
        if (t[i].classe==0 && t[i].dep==departement){
            pop=t[i].pop;
            trouve=true;
        }    
        i++;
    }
    return pop;
}


void triDate(tab t,int val){
    int j;
    covid temp;
    val=val/1.3; // Permet de diviser l'intervalle de la taille du tableau par 1.3 (c'est une division entière)
    while (val!=0){ // pour le dernier tour , val sera égal a 0 et donc c'est pour sa qu'ont l'enlève
        for (j=val;j<MAX;j++){
            
            if (strcmp(t[j-val].date,t[j].date)>0){ // regarde si le premier nombre a l'intervalle j-val est plus grand que l'intervalle j 
              temp=t[j-val];
              t[j-val]=t[j]; // permut les valeurs 
              t[j]=temp; 
            }
            
        }
    val=val/1.3; // nous divisons avant de relancer le while sinon la while fait une boucle infinie
    }
}

void DebutAnnee35(tab t){
    tableau temp;
    int i=0;
    int departement=35;
    bool trouve=false;
    
    for (int j=0;j<10;j++){
        temp[j]=0;
    }

    while (!trouve && i<MAX)
    {
        while (strcmp(t[i].date,"2021-01-01")==0)
        {
            if(t[i].dep==departement){
                
                if (t[i].classe==9)
                {
                    temp[0]=t[i].pos;
                }
                if (t[i].classe==19)
                {
                    temp[1]=t[i].pos;
                }
                if (t[i].classe==29)
                {
                    temp[2]=t[i].pos;
                }
                if (t[i].classe==39)
                {
                    temp[3]=t[i].pos;
                }
                if (t[i].classe==49)
                {
                    temp[4]=t[i].pos;
                }
                if (t[i].classe==59)
                {
                    temp[5]=t[i].pos;
                }
                if (t[i].classe==69)
                {
                    temp[6]=t[i].pos;
                }
                if (t[i].classe==79)
                {
                    temp[7]=t[i].pos;
                }
                if (t[i].classe==89)
                {
                    temp[8]=t[i].pos;
                }
                if (t[i].classe==90)
                {
                    temp[9]=t[i].pos;
                }
                if (t[i].classe==0)
                {
                    temp[10]=t[i].pos;
                }
            }
            trouve=true;
            i++;
        }
        i++;
    }
    printf("Nombre de cas en Ille-et-Vilaine par classe d'age, le 1er janvier 2021\n\n");
    printf("[ 0 -  9] : %d\n",temp[0]);
    printf("[10 - 19] : %d\n",temp[1]);
    printf("[20 - 29] : %d\n",temp[2]);
    printf("[30 - 39] : %d\n",temp[3]);
    printf("[40 - 49] : %d\n",temp[4]);
    printf("[50 - 59] : %d\n",temp[5]);
    printf("[60 - 69] : %d\n",temp[6]);
    printf("[70 - 79] : %d\n",temp[7]);
    printf("[80 - 89] : %d\n",temp[8]);
    printf("[90 - 99] : %d\n",temp[9]);
    printf("TOTAL : %d\n",temp[10]);
}

    
/* INITIALISE UN TABLEAU A PLUSIEURS DIMENSTIONS*/

void initDistance(tabDistances td){
    for(int i=0; i<MAX;i++){
        for(int j=0; j<MAX;j++){
            td[i][j]=0;
        }
    }
}

/* CREE UN FICHIER*/

void create(t_chaine50 nomFichier){
    FILE *fic;
    nom_type_structure abonne ;
    fic=fopen(nomFichier,"wb");
    printf("Donner le nom de l'abonné\n");
    scanf("%s",structure.élément);
    while (strcmp(structure.élément,"*")!=0){
        printf("Donner le numéro de l'abonnée\n");
        scanf("%s",structure.élément);
        printf("Donnez la durée pour laquelle il est abonné\n");
        scanf("%d",&structure.élément);
        fwrite(&structure,sizeof(nom_type_structure),1,fic);
        printf("Donnez le nom de l'abonné \n");
        scanf("%s",structure.élément);
        
    }
    fclose(fic);
}

/* LIRE UN FICHIER */

void vread(t_chaine50 nomFichier){
    
    FILE * fic ;
    t_abonne abonne;
    fic = fopen(nomFichier,"rb");
    
    if (fic!=NULL){
        
        fread(&abonne,sizeof(t_abonne),1,fic);
        printf("cc");
        while (!feof(fic))
            {
            printf("\nDonnez le nom , le numéro et la durée de cet abonné sont %s %s %d",abonne.c_nom,abonne.c_num,abonne.c_duree);
            fread(&abonne,sizeof(t_abonne),1,fic);
            }
        fclose (fic);
    }
    else {
        printf("ERROR");
    }
}

/* ECRIRE DANS UN FICHIER */

void write(t_chaine50 nomFichier){
    FILE * fic ;
    t_abonne abonne;
    fic = fopen(nomFichier,"wb");
    printf("Donnez le nom de l'abonné \n");
    scanf("%s",abonne.c_nom);
    while (strcmp(abonne.c_nom,"*")!=0){
        printf("Donner le numéro de l'abonnée\n");
        scanf("%s",abonne.c_num);
        printf("Donnez la durée pour laquelle il est abonné\n");
        scanf("%d",&abonne.c_duree);
        printf("Donnez le nom de l'abonné \n");
        scanf("%s",abonne.c_nom);
    }
    fclose(fic);
}

/* COPIER FICHIER DANS UN TABLEAU */

void remplirPlanete(tabPlanetes tp,int * nbPlanete){
    int i=0;
    
    FILE * fic ;
    tPlanete tp2;
    fic = fopen("Planetes.data","rb");
    fread(&tp2,sizeof(tPlanete),1,fic);

    while(!feof(fic)){
        tp[i]=tp2;
        i++;
        fread(&tp2,sizeof(tPlanete),1,fic);
    }
    *nbPlanete = i;
    fclose(fic);
}
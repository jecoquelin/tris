#include <stdio.h>
#include <stdlib.h>
#include <time.h>


#define N 50000   // Constante N mis à la valeur 10
typedef int tab[N] ;    // Définition d'un tableau de N entier


void croissant(tab t,int *comp,int *permut){  // Procédure qui tri par ordre croissant en ayant en paramètre le tableau t qui sera modifier (sauf si déjà trié)
    int i,j ; // Compteurs
    int temp; // Varriable temporaire
    for (i=0;i<N;i++){
        j=i;
        *comp=*comp+1;
        while ((j>0) && (t[j]<t[j-1])){
            temp=t[j];
            t[j]=t[j-1];
            t[j-1]=temp;
            j--;
            *permut=*permut+1;
            *comp=*comp+1;
        }
    }
}

void decroissant(tab t){ // Procédure qui tri par ordre décroissant en ayant en paramètre le tableau t qui sera modifier (sauf si déjè trié)
    int i,j ; // Compteurs
    int temp; // Variable temporaire
    for (i=0;i<N;i++){
        j=i;
        while ((j>0) && (t[j]>t[j-1])){
            temp=t[j];
            t[j]=t[j-1];
            t[j-1]=temp;
            j--;
        }
    }
}

// Procédure init qui permet de créer un tableau avec des nombres aléatoie
void init(tab t){
    int i;
    for (i=0;i<N;i++){
        t[i]= rand()%(RAND_MAX) ;   //Génère un nombre alétoire 
    }
}


int main(){ // Programme principal
    int nbcompm=0;
    int nbpermutm=0;
    int nbcompf=0;
    int nbpermutf=0;
    int nbcompd=0;
    int nbpermutd=0;
    tab t; //Inititialisation du tableau avec les N valeaurs
    init(t);
    printf("Tri par insertion :\n\n\n");
    

    printf("Cas Moyen :\n\n");

    printf("Début du tri croissant\n");
    
    clock_t b = clock();

    croissant(t,&nbcompm,&nbpermutm);

    clock_t e = clock();
    double  moyen = (e - b)*1.0 / CLOCKS_PER_SEC;
    
    printf("Fin du tri croissant\n\n");
    

    printf("Cas favorable :\n\n");

    printf("Début du tri\n");

    clock_t be = clock();

    croissant(t,&nbcompf,&nbpermutf);

    clock_t en = clock();
    double favo = (en - be)*1.0 / CLOCKS_PER_SEC;

    printf("Fin du tri\n\n");


    decroissant(t);
    printf("Cas défavorable :\n\n");

    printf("Début du tri\n");

    clock_t begin = clock();
    
    croissant(t,&nbcompd,&nbpermutd);

    clock_t end = clock();
    double defavo = (end - begin)*1.0 / CLOCKS_PER_SEC;
    printf("Fin du tri\n\n");
    printf("Le temps CPU du tri moyen = %.3f secondes\n",moyen);
    printf("Le temps CPU du tri favorable = %.3f secondes\n",favo);
    printf("Le temps CPU du tri défavorable = %.3f secondes\n",defavo);
    printf("\n\nLe nombre de comparaison du cas moyen = %d",nbcompm);
    printf("\nLe nombre de permutation du cas moyen = %d\n\n",nbpermutm);

    printf("\n\nLe nombre de comparaison du cas favorable = %d",nbcompf);
    printf("\nLe nombre de permutation du cas favorable = %d\n\n",nbpermutf);
    
    printf("\n\nLe nombre de comparaison du cas défavorable = %d",nbcompd);
    
    
    printf("\nLe nombre de permutation du cas défavorable = %d\n\n",nbpermutd);


}
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>


#define N 500000
typedef int tableau[N];

int aleatoire();
void ajouter(tableau tab);
void afficher(tableau tab);
void triPeigne(tableau tab,double val,int *comp,int *permut);
void triPeigneInverse(tableau tab,int val);

int main(){
  int nbcompm=0;
  int nbpermutm=0;
  int nbcompf=0;
  int nbpermutf=0;
  int nbcompd=0;
  int nbpermutd=0;

  srand(time(NULL));
  tableau tab;
  ajouter(tab);
  double i=N;

  printf("    TRI A PEIGNE    \n");

  printf("Cas Moyen :\n\n");

  printf("Début du tri croissant\n");
  clock_t b = clock();
  triPeigne(tab,i,&nbcompm,&nbpermutm);
  clock_t e = clock();
  double  moyen = (e - b)*1.0 / CLOCKS_PER_SEC;

  printf("Fin du tri croissant\n\n");

  printf("Cas favorable :\n\n");

  printf("Début du tri\n");
  clock_t be = clock();
  triPeigne(tab,i,&nbcompf,&nbpermutf);

  clock_t en = clock();
  double favo = (en - be)*1.0 / CLOCKS_PER_SEC;
  printf("Fin du tri\n\n");

  triPeigneInverse(tab,i);
  printf("Cas défavorable :\n\n");

  printf("Début du tri\n");
  clock_t begin = clock();
  triPeigne(tab,i,&nbcompd,&nbpermutd);
  clock_t end = clock();
  double defavo = (end - begin)*1.0 / CLOCKS_PER_SEC;
  printf("Le temps CPU du tri moyen = %.3f secondes\n",moyen);
  printf("Le temps CPU du tri favorable = %.3f secondes\n",favo);
  printf("Le temps CPU du tri défavorable = %.3f secondes\n",defavo);
  printf("\n\nLe nombre de comparaison du cas moyen = %d",nbcompm);
  printf("\nLe nombre de permutation du cas moyen = %d\n\n",nbpermutm);

  printf("\n\nLe nombre de comparaison du cas favorable = %d",nbcompf);
  printf("\nLe nombre de permutation du cas favorable = %d\n\n",nbpermutf);
    
  printf("\n\nLe nombre de comparaison du cas défavorable = %d",nbcompd);
    
    
  printf("\nLe nombre de permutation du cas défavorable = %d\n\n",nbpermutd);
}

int aleatoire(){
  int res;
  res=rand() % (RAND_MAX) ;
  return res ;
}

void ajouter(tableau tab){
  int val;
  for (int i=0;i<N;i++){
    val=aleatoire();
    tab[i]=val;
  }
}

void afficher(tableau tab){
  for (int i=0;i<N;i++){
    printf("%d ",tab[i]);
  }
}


/**
 * \brief procédure permettant de trier en mode croissant un tableau de la façon tri peigne 
 * 
 * \param tab : représente un tableau de valeurs
 * \param val : représente l'intervalle de la taille du tableau 
 */
void triPeigne(tableau tab,double val,int *comp,int *permut){
    int j;
    int temp;
    int valentiere;

    val=val/1.3;
    valentiere =(int)round(val); // Permet de diviser l'intervalle de la taille du tableau par 1.3 (c'est une division entière)
    while (valentiere!=0){ // pour le dernier tour , val sera égal a 0 et donc c'est pour sa qu'ont l'enlève
        for (j=valentiere;j<N;j++){
            *comp=*comp+1;
            if (tab[j-valentiere]>tab[j]){ // regarde si le premier nombre a l'intervalle j-val est plus grand que l'intervalle j 
                
                temp = tab[j-valentiere];  // permut les valeurs 
                tab[j-valentiere] = tab[j];
                tab[j]= temp;
                *permut=*permut+1;
            }
            
        }
    val=val/1.3;
    valentiere=(int)round(val); // nous divisons avant de relancer le while sinon la while fait une boucle infinie
    }
}

/**
 * @brief procédure permettant de trier en mode décroissant un tableau de la façon tri peigne 
 * 
 * @param tab : représente un tableau de valeurs
 * @param val : représente l'intervalle de la taille du tableau 
 */
void triPeigneInverse(tableau tab,int val){
    int j;
    int temp;
    val=val/1.3; // Permet de diviser l'intervalle de la taille du tableau par 1.3 (c'est une division entière)
    while (val!=0){ // pour le dernier tour , val sera égal a 0 et donc c'est pour sa qu'ont l'enlève
        for (j=val;j<N;j++){
            
            if (tab[j-val]<tab[j]){  // regarde si le premier nombre a l'intervalle j-val est plus petit que l'intervalle j 
                
                temp = tab[j-val];  // permut les valeurs 
                tab[j-val] = tab[j];
                tab[j]= temp;
                
            }
            
        }
    val=val/1.3; // nous divisons avant de relancer le while sinon la while fait une boucle infinie
    }
}

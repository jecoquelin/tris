#include <stdio.h>
#include <stdlib.h>
#include <time.h>


#define N 500000
typedef int tableau[N];

int aleatoire();
void ajouter(tableau tab);
void afficher(tableau tab);
void triFusion(int i , int j , tableau tab, tableau tmp,int *comp,int *permut);
void triFusionInverse(int i , int j , tableau tab, tableau tmp);


int main(){
  int nbcompm=0;
  int nbpermutm=0;
  int nbcompf=0;
  int nbpermutf=0;
  int nbcompd=0;
  int nbpermutd=0;

  srand(time(NULL));
  tableau tab;
  tableau tmp;
  ajouter(tab);
  

  int i=0;
  int j=N-1;
  printf("    TRI FUSION    \n");

  printf("Cas Moyen :\n\n");

  printf("Début du tri croissant\n");

  clock_t b = clock();

  triFusion(i,j,tab,tmp,&nbcompm,&nbpermutm);

  clock_t e = clock();
  double  moyen = (e - b)*1.0 / CLOCKS_PER_SEC;

  printf("Fin du tri croissant\n\n");

  printf("Cas favorable :\n\n");

  printf("Début du tri\n");

  clock_t be = clock();

  triFusion(i,j,tab,tmp,&nbcompf,&nbpermutf);

  clock_t en = clock();
  double favo = (en - be)*1.0 / CLOCKS_PER_SEC;

  printf("Fin du tri\n\n");

  triFusionInverse(i,j,tab,tmp);
  printf("Cas défavorable :\n\n");

  printf("Début du tri\n");
  clock_t begin = clock();
  triFusion(i,j,tab,tmp,&nbcompd,&nbpermutd);
  clock_t end = clock();

  double defavo = (end - begin)*1.0 / CLOCKS_PER_SEC;
  printf("Le temps CPU du tri moyen = %.3f secondes\n",moyen);
  printf("Le temps CPU du tri favorable = %.3f secondes\n",favo);
  printf("Le temps CPU du tri défavorable = %.3f secondes\n",defavo);
  printf("\n\nLe nombre de comparaison du cas moyen = %d",nbcompm);
  printf("\nLe nombre de permutation du cas moyen = %d\n\n",nbpermutm);

  printf("\n\nLe nombre de comparaison du cas favorable = %d",nbcompf);
  printf("\nLe nombre de permutation du cas favorable = %d\n\n",nbpermutf);
    
  printf("\n\nLe nombre de comparaison du cas défavorable = %d",nbcompd);
    
    
  printf("\nLe nombre de permutation du cas défavorable = %d\n\n",nbpermutd);
}

int aleatoire(){
  int res;
  res=rand() % (RAND_MAX)+1 ;
  return res ;
}

void ajouter(tableau tab){
  int val;
  for (int i=0;i<N;i++){
    val=aleatoire();
    tab[i]=val;
  }
}

void afficher(tableau tab){
  for (int i=0;i<N;i++){
    printf("%d ",tab[i]);
  }
}

void triFusion(int i, int j ,tableau tab, tableau tmp,int *comp,int *permut){
    if (j<=i){
        return;
    }
    
    int m = (i+j)/2;

    triFusion(i,m,tab,tmp,&*comp,&*permut);     //trier la moitié gauche récursivement
    triFusion(m+1,j,tab,tmp,&*comp,&*permut);   //trier la moitié droite récursivement
    int pg = i;     //pg pointe au début du sous-tableau de gauche
    int pd = m+1;   //pd pointe au début du sous-tableau de droite
    int cmp;        //compteur
// on boucle de i à j pour remplir chaque élément du tableau final fusionné
    for (cmp=i;cmp<= j;cmp++){
        if (pg == m+1){ //le pointeur du sous-tableau de gauche a atteint la limite
            tmp[cmp] = tab[pd];
            pd++;
            *comp=*comp+1;
            *permut=*permut+1;
            
        }
        else if (pd == j+1){    //le pointeur du sous-tableau de droite a atteint la limite
            tmp[cmp] = tab[pg];
            pg++;
            *comp=*comp+2;
            *permut=*permut+1;
        }
        else if (tab[pg]< tab[pd]){ //le pointeur du sous-tableau de gauche pointe vers un élément plus petit
            tmp[cmp] = tab[pg];
            pg++;
            *comp=*comp+3;
            *permut=*permut+1;
        }
        else {  //le pointeur du sous-tableau de droite pointe vers un élément plus petit
            tmp[cmp] = tab[pd];
            pd++;
            *comp=*comp+3;
            *permut=*permut+1;
        }
        
    }
    for(cmp = i; cmp <= j; cmp++){ //copier les éléments de tmp[] à tab[]
        
        tab[cmp] = tmp[cmp];
    }
}

void triFusionInverse(int i, int j ,tableau tab, tableau tmp ){
    if (j<=i){
        return;
    }

    int m = (i+j)/2;
    triFusionInverse(i,m,tab,tmp);     //trier la moitié gauche récursivement
    triFusionInverse(m+1,j,tab,tmp);   //trier la moitié droite récursivement
    int pg = i;     //pg pointe au début du sous-tableau de gauche
    int pd = m+1;   //pd pointe au début du sous-tableau de droite
    int cmp;        //compteur
// on boucle de i à j pour remplir chaque élément du tableau final fusionné
    for (cmp=i;cmp<= j;cmp++){
        if (pg == m+1){ //le pointeur du sous-tableau de gauche a atteint la limite
            tmp[cmp] = tab[pd];
            pd++;
        }
        else if (pd == j+1){    //le pointeur du sous-tableau de droite a atteint la limite
            tmp[cmp] = tab[pg];
            pg++;
        }
        else if (tab[pg]> tab[pd]){ //le pointeur du sous-tableau de gauche pointe vers un élément plus grand
            tmp[cmp] = tab[pg];
            pg++;
        }
        else {  //le pointeur du sous-tableau de droite pointe vers un élément plus petit
            tmp[cmp] = tab[pd];
            pd++;
        }
    }
    for(cmp = i; cmp <= j; cmp++){ //copier les éléments de tmp[] à tab[]
        tab[cmp] = tmp[cmp];
    }
}